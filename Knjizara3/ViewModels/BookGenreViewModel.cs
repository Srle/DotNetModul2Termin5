﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Knjizara3.Models;

namespace Knjizara3.ViewModels
{
    public class BookGenreViewModel
    {
        public Book Book { get; set; }
        public List<Genre> Genres { get; set; }
        public int SelectedGenreId { get; set; }

    }
}
