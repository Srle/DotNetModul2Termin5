﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Knjizara3.Models;
using Knjizara3.ViewModels;

namespace Knjizara3.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddBookstore(string NameOfBookstore)
        {
            int id = Project.Allbookstores.Count + 1;

            Bookstore bs = new Bookstore(id, NameOfBookstore);
            Project.Allbookstores.Add(bs);

            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View(Project.Allbookstores);
        }

        public ActionResult Books( int id)
        {
            Project.st = id - 1;
            return RedirectToAction("List", "Book");
        }

    }





































}