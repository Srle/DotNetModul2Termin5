﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Knjizara3.Models;
using Knjizara3.ViewModels;

namespace Knjizara3.Controllers
{
    public class BookController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            return View(Project.AllGenres);
        }


        [HttpPost]
        public ActionResult AddBook(string NameOfBook, double PriceOfBook, int IdGenreOfBook)
        {
            int id = Project.Allbookstores[Project.st].Books.Count + 1;
            Book bk = new Book(id, NameOfBook, PriceOfBook, false);
    
            foreach (Genre gr in Project.AllGenres)    // na osnovu vrednosti Id-ja koji smo izabrali trazimo zanr
            {
                if (gr.Id == IdGenreOfBook)
                {
                    bk.Bookgenre = gr;  // kada smo nasli, dodeljujemo knjizi zanr
                }
            }

            Project.Allbooks.Add(bk);
            Project.Allbookstores[Project.st].Books.Add(bk);

            return RedirectToAction("List");
        }


        public ActionResult List(string sortby)
        {
            switch (sortby)
            {
                case "NameUp":
                    var AllBooksOrderedByNameUp = Project.Allbookstores[Project.st].Books.OrderBy(w => w.Name);
                    return View(AllBooksOrderedByNameUp);

                case "NameDown":
                    var AllBooksOrderedByNameDown = Project.Allbookstores[Project.st].Books.OrderByDescending(w => w.Name);
                    return View(AllBooksOrderedByNameDown);

                case "PriceUp":
                    var AllBooksOrderedByPriceUp = Project.Allbookstores[Project.st].Books.OrderBy(w => w.Price);
                    return View(AllBooksOrderedByPriceUp);

                case "PriceDown":
                    var AllBooksOrderedByPriceDown = Project.Allbookstores[Project.st].Books.OrderByDescending(w => w.Price);
                    return View(AllBooksOrderedByPriceDown);

                default:
                    return View(Project.Allbookstores[Project.st].Books);

            }

        }



        public ActionResult Delete(int id)
        {
            for (int i = 0; i < Project.Allbookstores[Project.st].Books.Count; i++)
            {
                if (Project.Allbookstores[Project.st].Books[i].Id == id)
                {
                    Project.Allbookstores[Project.st].Books[i].Deleted = true;
                }
            }

            return RedirectToAction("Deleted");
        }


        public ActionResult Deleted()
        {
            return View(Project.Allbookstores[Project.st].Books);
        }


        public ActionResult Edit(int id)
        {
            Book nadjena = new Book();
            BookGenreViewModel vm = new BookGenreViewModel();
            foreach (Book bk in Project.Allbooks)
            {
                if (bk.Id == id)
                {
                    nadjena = bk;
                }
            }
            vm.Book = nadjena;
            vm.Genres = Project.AllGenres;
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(BookGenreViewModel vm)
        {
            Genre newGenre = new Genre();

            foreach (var bookGenre in Project.AllGenres)
            {
                if (bookGenre.Id == vm.SelectedGenreId)
                {
                    newGenre = bookGenre;
                }
            }

            foreach (var Bk in Project.Allbooks)
            {
                if (Bk.Id == vm.Book.Id)
                {
                    Bk.Name = vm.Book.Name;
                    Bk.Price = vm.Book.Price;
                    Bk.Deleted = false;
                    Bk.Bookgenre = newGenre;
                }
            }
            return RedirectToAction("List");
        }

    }
}