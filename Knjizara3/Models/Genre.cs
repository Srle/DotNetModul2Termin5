﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara3.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public List<Book> GenreBooks { get; set; }

        public Genre()
        {
        }

        public Genre(int id, string name, bool deleted)
        {
            GenreBooks = new List<Book>();
            this.Id = id;
            this.Name = name;
            this.Deleted = deleted;
        }

        public Genre(int id, string name, bool deleted, List<Book> books)
        {
            this.Id = id;
            this.Name = name;
            this.Deleted = deleted;
            this.GenreBooks = books;
        }
    }
}